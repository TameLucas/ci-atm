﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CI_ATM.data
{
    class AutoServicoCrawler
    {
        
        private string AS_URL = "https://www.ufpb.br/AutoServico/AuthDiscente";
        private string DISC_URL = "https://www.ufpb.br/AutoServico/Discente";
        private string matricula, senha, ctrl;
        private HttpClient client;
        private bool loggedIn;

        public AutoServicoCrawler(string matricula, string senha)
        {
            this.matricula = matricula;
            this.senha = senha;
            this.loggedIn = false;
            client = new HttpClient();
        }

        public bool isLoggedIn()
        {
            return loggedIn;
        }

        public async Task loginToUFPB()
        {
            // Cria o HttpContent para executar o POST
            FormUrlEncodedContent requestContent = new FormUrlEncodedContent(new[] {
            new KeyValuePair<string, string>("matricula", matricula),
            new KeyValuePair<string, string>("senha", senha),
            });

            // Obtém a resposta e o conteudo dela
            HttpResponseMessage response = await client.PostAsync(AS_URL, requestContent);
            HttpContent responseContent = response.Content;

            // Pega o conteudo
            using (var reader = new StreamReader(await responseContent.ReadAsStreamAsync()))
            {
                string line = await reader.ReadToEndAsync();
                MessageBox.Show(line);
                // Verifica se a chave de Login existe
                if (line.Contains("ctrl"))
                {
                    // Captura a chave de Login
                    Match r = Regex.Match(line, "id=\"ctrl\" value=\"(.*)\"", RegexOptions.IgnoreCase);
                    if (r.Success)
                    {
                        // Armazena a chave de login e define como logado
                        ctrl = r.Groups[1].Value.ToString();
                        loggedIn = true;
                    }
                }
            }
        }

        public async Task<string> getContent(string opcao)
        {
            if (loggedIn)
            {
                // Cria o HttpContent para executar o POST
                FormUrlEncodedContent requestContent = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("ctrl", ctrl),
                    new KeyValuePair<string, string>("opcao", opcao),
                });

                // Obtém a resposta e o conteudo dela
                HttpResponseMessage resp = await client.PostAsync(DISC_URL, requestContent);
                HttpContent content = resp.Content;

                using (var rdr = new StreamReader(await content.ReadAsStreamAsync()))
                {
                    // Escreve o output
                    string line = await rdr.ReadToEndAsync();
                    return line;
                }

            }
            return "IH, DEU ERRO.";
        }

    }
}
