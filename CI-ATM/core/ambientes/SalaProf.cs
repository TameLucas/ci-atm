using System.Collections.Generic;

namespace CI_ATM.core
{
    public class SalaProf : Ambiente
    {
        private List<Professor> professores;

        public SalaProf(string id, string nome, string desc, string site) : base(id, nome, desc)
        {
            professores = new List<Professor>();
        }

        public void addProfessor(Professor p)
        {
            professores.Add(p);
        }
    }
}