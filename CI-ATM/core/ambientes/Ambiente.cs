namespace CI_ATM.core
{
    public class Ambiente
    {
        public Ambiente(string id, string nome, string desc)
        {
            this.id = id;
            this.nome = nome;
            this.descricao = desc;
        }

        private string id, nome, descricao;

        public string getId()
        {
            return id;
        }

        public void setId(string id)
        {
            this.id = id;
        }

        public string getNome()
        {
            return nome;
        }

        public void setNome(string nome)
        {
            this.nome = nome;
        }

        public string getDesc()
        {
            return descricao;
        }

        public void setDesc(string desc)
        {
            this.descricao = desc;
        }
    }
}