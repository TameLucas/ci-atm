using System.Collections.Generic;
using System.Drawing;

namespace CI_ATM.core
{
    public class LabPesquisa : Ambiente
    {
        private List<Professor> professores;
        private string site;
        private Image logo;

        public LabPesquisa(string id, string nome, string desc, string site) : base(id, nome, desc)
        {
            this.site = site;
            this.logo = null;
            professores = new List<Professor>();
        }

        public string getSite()
        {
            return site;
        }

        public void setSite(string site)
        {
            this.site = site;
        }

        public Image getLogo()
        {
            return logo;
        }
        public void setLogo(Image p)
        {
            this.logo = p;
        }

        public void addProfessor(Professor p)
        {
            professores.Add(p);
        }
    }
}