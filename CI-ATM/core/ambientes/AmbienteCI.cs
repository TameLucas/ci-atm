using System.Collections.Generic;

namespace CI_ATM.core
{
    public class AmbienteCI : Ambiente
    {
	    private List<Funcionario> funcionarios;

        public AmbienteCI(string id, string nome, string desc) : base(id, nome, desc)
        {
            funcionarios = new List<Funcionario>();
        }

        public void addFuncionario(Funcionario p)
	    {
		    funcionarios.Add(p);
	    }
    }
}