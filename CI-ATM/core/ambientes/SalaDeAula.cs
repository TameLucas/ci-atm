namespace CI_ATM.core
{
    public class SalaDeAula : Ambiente
    {
        private int capacidade, computadores;

        public enum TipoSala
        {
            SalaComum,
            Laboratorio,
            LabEletr,
            LabRobotica
        }

        private TipoSala subtipo;
        private bool dataShow;

        public SalaDeAula(string id, string nome, string desc, TipoSala tipo, int cap, int comp, bool ds) : base(id, nome, desc)
        {
            this.subtipo = tipo;
            this.capacidade = cap;
            this.computadores = comp;
            this.dataShow = ds;
        }

        public int getCapacidade()
        {
            return capacidade;
        }

        public void setCapacidade(int cap)
        {
            this.capacidade = cap;
        }

        public int getQtdComputadores()
        {
            return computadores;
        }

        public void setQtdComputadores(int comp)
        {
            this.computadores = comp;
        }

        public TipoSala getTipo()
        {
            return subtipo;
        }

        public void setTipo(TipoSala tipo)
        {
            subtipo = tipo;
        }
    }
}