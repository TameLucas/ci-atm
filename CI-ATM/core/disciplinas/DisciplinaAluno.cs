﻿namespace CI_ATM.core
{
    public class DisciplinaAluno : DisciplinaSemestre
    {
        public enum STATUS
        {
            REPROVADO,
            APROVADO,
            CURSANDO,
            TRANCADO
        }

        private STATUS status; 

        public DisciplinaAluno(int code, string nome, string ementa, int creditos, float periodo, STATUS st, float nota) : base(code, nome, ementa, creditos, periodo)
        {
            this.status = st;
            this.nota = nota;
        }

        private float nota;

        public float getNota()
        {
            return nota;
        }

        public void setNota(float nota)
        {
            this.nota = nota;
        }

    }
}
