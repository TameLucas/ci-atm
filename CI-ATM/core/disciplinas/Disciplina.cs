﻿namespace CI_ATM.core
{
    public class Disciplina
    {
        public int code, creditos;
        private string nome, ementa;

        public Disciplina(int code, string nome, string ementa, int creditos)
        {
            this.code = code;
            this.nome = nome;
            this.ementa = ementa;
            this.creditos = creditos;
        }

        public int getCodigo()
        {
            return code;
        }

        public void setCodigo(int code)
        {
            this.code = code;
        }

        public int getCreditos()
        {
            return creditos;
        }

        public void setCreditos(int creditos)
        {
            this.creditos = creditos;
        }

        public int getCargaHoraria()
        {
            return code * 15;
        }

        public string getNome()
        {
            return nome;
        }

        public void setNome(string nome)
        {
            this.nome = nome;
        }

        public string getEmenta()
        {
            return ementa;
        }

        public void setEmenta(string ementa)
        {
            this.ementa = ementa;
        }
    }
}
