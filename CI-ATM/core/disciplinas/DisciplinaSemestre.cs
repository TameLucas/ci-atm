﻿using System;
using System.Collections.Generic;

namespace CI_ATM.core
{
    public class DisciplinaSemestre : Disciplina
    {
        private float periodo;
        private List<DayOfWeek> dias;
        private List<DateTime> startTime;

        public DisciplinaSemestre(int code, string nome, string ementa, int creditos, float periodo) : base(code, nome, ementa, creditos)
        {
            this.periodo = periodo;
            dias = new List<DayOfWeek>();
            startTime = new List<DateTime>();
        }

        public float getPeriodo()
        {
            return periodo;
        }

        public void setPeriodo(float periodo)
        {
            this.periodo = periodo;
        }

        public List<DayOfWeek> getDaysOfWeek()
        {
            return dias;
        }

        public void addDia(DayOfWeek dia, DateTime horario)
        {
            dias.Add(dia);
            startTime.Add(horario);
        }

        public void addDia(List<DayOfWeek> dias, List<DateTime> horarios)
        {
            dias.AddRange(dias);
            startTime.AddRange(horarios);
        }
    }
}
