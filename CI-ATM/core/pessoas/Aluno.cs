using System.Collections.Generic;

namespace CI_ATM.core
{
    public class Aluno : Pessoa
    {
        private List<DisciplinaAluno> disciplinas;
        private float CRE;
        private Curso curso;

        public enum Curso
        {
            CC,
            EC,
            MC,
            PPGI
        }

        public Aluno(string nome, int matricula, Curso curso, float CRE) : base(nome, matricula)
        {
            this.CRE = CRE;
            this.curso = curso;
            disciplinas = new List<DisciplinaAluno>();
        }

        public float getCRE()
        {
            return CRE;
        }

        public void setCRE(float CRE)
        {
            this.CRE = CRE;
        }

        public void addDisciplina(DisciplinaAluno d)
        {
            disciplinas.Add(d);
        }

        public Curso getCurso()
        {
            return curso;
        }

        public void setCurso(Curso c)
        {
            this.curso = c;
        }
    }
}