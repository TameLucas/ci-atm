namespace CI_ATM.core
{
    public class Pessoa
    {
        private string nome;
        private int matricula;

        protected Pessoa(string nome, int matricula)
        {
            this.nome = nome;
            this.matricula = matricula;
        }

        public string getNome()
        {
            return nome;
        }

        public void setNome(string nome)
        {
            this.nome = nome;
        }

        public int getMatricula()
        {
            return matricula;
        }

        public void setMatricula(int matricula)
        {
            this.matricula = matricula;
        }
    }
}