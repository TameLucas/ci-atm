using System.Collections.Generic;

namespace CI_ATM.core
{
    public class Professor : Pessoa
    {
	    private List<DisciplinaSemestre> disciplinas;
	    private string email, lattes;
	
        public Professor(string nome, int matricula, string email, string lattes) : base(nome, matricula)
        {
            this.email = email;
            this.lattes = lattes;
            disciplinas = new List<DisciplinaSemestre>();
        }

	    public string getEmail()
	    {
		    return email;
	    }
	
	    public void setEmail(string email)
	    {
		    this.email = email;
	    }
	
	    public string getLattes()
	    {
		    return lattes;
	    }
	
	    public void setLattes(string lattes)
	    {
		    this.lattes = lattes;
	    }
	
	    public void addDisciplina(DisciplinaSemestre d)
	    {
		    disciplinas.Add(d);
	    }
    }
}