﻿namespace CI_ATM.core
{
    public class Funcionario : Pessoa
    {
        private string funcao;

        public Funcionario(string nome, int matricula, string funcao) : base(nome, matricula)
        {
            this.funcao = funcao;
        }

        public string getFuncao()
        {
            return funcao;
        }
        public void setFuncao(string func)
        {
            this.funcao = func;
        }
    }
}
