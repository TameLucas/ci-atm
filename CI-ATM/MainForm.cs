﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CI_ATM.core;
using CI_ATM.data;

namespace CI_ATM
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        public bool abc = false;
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (!abc)
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                TopMost = true;
                abc = true;
            }
            else
            {
                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
                TopMost = false;
                abc = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AutoServicoCrawler asc = new AutoServicoCrawler("11318608", "04011996");
            Task.Run(() => asc.loginToUFPB()).Wait();
            if (asc.isLoggedIn())
            {
                MessageBox.Show("done");
                string line = Task.Run(() => asc.getContent("2")).Result;
                MessageBox.Show(line);
            }

        }
    }
}
